
public class Aufgabe1 {

	public static void main(String[] args) {
		
		String s = "*";
		
		System.out.printf( "%3s" + "%s \n", s , s );
		System.out.printf( "%-5s" + "%s\n", s , s );
		System.out.printf( "%-5s" + "%s\n", s , s );
		System.out.printf( "%3s" + "%s \n", s , s );
		
	}

}
