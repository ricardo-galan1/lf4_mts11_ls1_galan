package Konsoleneingabe;

import java.util.Scanner;

public class aufgabe2Konsoleneingabe {

	public static void main(String[] args) {
		Scanner myScanner= new Scanner(System.in);
		
		System.out.println("Hallo, wie ist dein Name?");
		String name = myScanner.next();
		
		System.out.println("\nWie alt bist du?");
		int alter = myScanner.nextInt();
		
		System.out.println("\n\nName: " + name + "\nAlter: " + alter);
		
		myScanner.close();

	}

}
