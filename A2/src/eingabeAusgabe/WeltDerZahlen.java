package eingabeAusgabe;

/**
 *   Aufgabe:  Recherechieren Sie im Internet !
 * 
 *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
 *
 *   Vergessen Sie nicht den richtigen Datentyp !!
 *
 *
 * @version 1.0 from 21.08.2019
 * @author << Ricardo Galan >>
 */

public class WeltDerZahlen {

 public static void main(String[] args) {
   
   /*  *********************************************************
   
        Zuerst werden die Variablen mit den Werten festgelegt!
   
   *********************************************************** */
   // Im Internet gefunden ?
   // Die Anzahl der Planeten in unserem Sonnesystem                    
   int anzahlPlaneten =  9;
   
   // Anzahl der Sterne in unserer Milchstra�e
   long anzahlSterne = 150000000000l;
   
   // Wie viele Einwohner hat Berlin?
   int bewohnerBerlin = 3645000;
   
   // Wie alt bist du?  Wie viele Tage sind das?
   
   int alterTage = 11484;
   
   // Wie viel wiegt das schwerste Tier der Welt?
   // Schreiben Sie das Gewicht in Kilogramm auf!
   int gewichtKilogramm = 150000;  
   
   // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
   int flaecheGroessteLand = 17130000;
   
   // Wie gro� ist das kleinste Land der Erde?
   
   float flaecheKleinsteLand = 0.4f; // in km�
   
   
   
   
   /*  *********************************************************
   
        Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
   
   *********************************************************** */
   
   System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
   System.out.println("Anzahl der Sterne in der Milchstra�e: " + anzahlSterne);
   System.out.println("Einwohnerzahl Berlin: "+ bewohnerBerlin);
   System.out.println("Mein Alter in Tagen: "+alterTage);
   System.out.println("Gewicht des schwersten Tiers der Welt: "+gewichtKilogramm + "kg");
   System.out.println("Flaeche des groesten Landes der Welt: "+"flaecheGroessteLand"+"km�");
   System.out.println("Flaeche des kleinsten Landes der Welt: "+flaecheKleinsteLand+"km�");
   System.out.println(" *******  Ende des Programms  ******* ");
   
 }
}